---
title: "Home"
draft: false
---

# Bem-vindo ao Meu Portfólio

Este site foi criado para apresentar minhas habilidades e projetos.

### O que você vai encontrar aqui:
- **Minha Biografia**: Conheça um pouco sobre mim.
- **Meus Projetos**: Veja alguns dos meus trabalhos mais recentes.
- **Meu Currículo**: Um resumo das minhas qualificações e experiência.

Explore e fique à vontade para entrar em contato!
